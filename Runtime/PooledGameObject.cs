using UnityEngine;

namespace OT.ObjectPool
{
    public abstract class PooledGameObject : MonoBehaviour
    {
        protected System.Action<PooledGameObject> OnRelease;
        public string PoolId => gameObject.name; // todo: Think about: convert string to int-hash.

        public bool IsPooled { get; set; }

        public virtual void Init(System.Action<PooledGameObject> onRelease)
        {
            OnRelease = onRelease;
        }

        public virtual void Release()
        {
            OnRelease.Invoke(this);
        }
    }
}

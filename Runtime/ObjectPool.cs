using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OT.ObjectPool
{
    public class ObjectPool : MonoBehaviour
    {
        #region Fields

        /// <summary> Contains prefab of pool objects. </summary>
        [SerializeField] private List<PooledGameObject> poolObjectPrefabs;
        [SerializeField] private int defaultBufferAmount = 1;

        private Dictionary<string, List<PooledGameObject>> poolingObjects =
            new Dictionary<string, List<PooledGameObject>>(1);

        private GameObject objectContainer;

        public Action<PooledGameObject> onGetPooled;
        public Action<PooledGameObject> onPutPooled;
        public static ObjectPool Instance;

        #endregion

        #region Props

        public Dictionary<long, PooledGameObject> pooledObjects { get; private set; }

        #endregion

        private void Awake()
        {
            Instance = this;
        }

        private static ObjectPool CreateNewPool(string poolName, int bufferAmount, Transform parent = null)
        {
            ObjectPool thePool = new GameObject(poolName).AddComponent<ObjectPool>();
            thePool.StartPool(bufferAmount);
            thePool.transform.SetParent(parent);
            return thePool;
        }

        private void InstantiateObjectByAddedPrefab(PooledGameObject objectPrefab, int amountCount = -1)
        {
            if (poolingObjects.ContainsKey(objectPrefab.PoolId) == false)
                poolingObjects.Add(objectPrefab.PoolId, new List<PooledGameObject>());

            int prefabsAddingCount = defaultBufferAmount;
            if (amountCount > 0)
                prefabsAddingCount = amountCount;

            while (poolingObjects[objectPrefab.PoolId].Count < prefabsAddingCount)
            {
                var newPooledObject = InstantiateNewObject<PooledGameObject>(objectPrefab);
                newPooledObject.gameObject.SetActive(false);
            }
        }

        private T InstantiateNewObject<T>(PooledGameObject objectPrefab) where T : PooledGameObject
        {
            GameObject newGo = Instantiate(objectPrefab.gameObject, objectContainer.transform);
            PooledGameObject objectPoolComponent = newGo.GetComponent<PooledGameObject>();
            newGo.name = objectPrefab.PoolId;
            objectPoolComponent.IsPooled = true;

            objectPoolComponent.Init(ReturnObjectToPool);
            ReturnObjectToPool(objectPoolComponent);

            return newGo.GetComponent<T>();
        }

        private T GetPooledObject<T>(PooledGameObject poolObjectPrototype) where T : PooledGameObject
        {
            var pooledPreloadedObject = GetPreloadedPooledObject<T>(poolObjectPrototype.PoolId);
            if (pooledPreloadedObject != null)
                return pooledPreloadedObject;

            poolObjectPrefabs.Add(poolObjectPrototype);
            InstantiateNewObject<T>(poolObjectPrototype);
            return GetPooledObject<T>(poolObjectPrototype);
        }

        private T GetPooledObject<T>(string nameOfPoolObj) where T : PooledGameObject
        {
            PooledGameObject pooledObject = poolingObjects[nameOfPoolObj][0];
            poolingObjects[nameOfPoolObj].RemoveAt(0);

            pooledObject.gameObject.SetActive(true);
            pooledObject.IsPooled = false;

            if (pooledObjects.ContainsKey(pooledObject.GetInstanceID()) == false)
                pooledObjects.Add(pooledObject.GetInstanceID(), pooledObject);

            onGetPooled?.Invoke(pooledObject);
            return pooledObject.GetComponent<T>();
        }

        public void StartPool(int bufferAmount = -1)
        {
            if (bufferAmount > 0)
                defaultBufferAmount = bufferAmount;

            if (objectContainer != null)
            {
                Debug.LogWarning("The pool is already started");
                return;
            }

            objectContainer = new GameObject(gameObject.name + "_container");
            objectContainer.transform.SetParent(transform);
            if (poolObjectPrefabs == null)
            {
                poolObjectPrefabs = new List<PooledGameObject>();
            }

            pooledObjects = new Dictionary<long, PooledGameObject>();
            foreach (var objectPrefab in poolObjectPrefabs)
            {
                InstantiateObjectByAddedPrefab(objectPrefab);
            }
        }

        /// <summary> Scripting approach to register object prefab to pool. </summary>
        /// <param name="objectPrefab">prefab obj</param>
        /// <param name="amountCount">amount instances</param>
        public void RegisterPoolObjectPrefab(PooledGameObject objectPrefab, int amountCount = -1)
        {
            if (poolObjectPrefabs.Contains(objectPrefab) == false)
                poolObjectPrefabs.Add(objectPrefab);

            InstantiateObjectByAddedPrefab(objectPrefab, amountCount);
        }

        /// <summary>
        /// Get object from pool.
        /// </summary>
        /// <param name="poolObjectId">Object name id</param>
        /// <typeparam name="T">Pooled Object type.</typeparam>
        /// <returns>Pooled Object type</returns>
        public T GetPreloadedPooledObject<T>(string poolObjectId) where T : PooledGameObject
        {
            // todo: revise this slow search approach.
            for (int i = 0; i < poolObjectPrefabs.Count; i++)
            {
                if (poolObjectPrefabs[i].PoolId == poolObjectId)
                {
                    if (poolingObjects.ContainsKey(poolObjectId) && poolingObjects[poolObjectId].Count > 0)
                    {
                        return GetPooledObject<T>(poolObjectId);
                    }
                    else
                    {
                        var newObject = InstantiateNewObject<T>(poolObjectPrefabs[i]);
                        return GetPooledObject<T>(newObject);
                    }
                }
            }

            return null;
        }

        public void ReturnObjectToPool(PooledGameObject pooledObject)
        {
            pooledObject.IsPooled = true;
            for (int i = 0; i < poolObjectPrefabs.Count; i++)
            {
                if (poolObjectPrefabs[i].PoolId == pooledObject.PoolId)
                {
                    if (poolingObjects.ContainsKey(pooledObject.PoolId) == false)
                        poolingObjects.Add(pooledObject.PoolId, new List<PooledGameObject>());

                    poolingObjects[pooledObject.PoolId].Add(pooledObject);
                    pooledObject.gameObject.SetActive(false);
                }
            }

            if (pooledObjects.ContainsKey(pooledObject.GetInstanceID()))
                pooledObjects.Remove(pooledObject.GetInstanceID());

            onPutPooled?.Invoke(pooledObject);
        }

        public void PoolAllObjects()
        {
            while (pooledObjects.Count > 0)
            {
                ReturnObjectToPool(pooledObjects.First().Value);
            }
        }

        public void StopPool(bool selfDestroy = false)
        {
            if (this == false)
                return;

            PoolAllObjects();
            if (Application.isPlaying)
            {
                Destroy(objectContainer);
                if (selfDestroy && this == true)
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                DestroyImmediate(objectContainer);
                if (selfDestroy && this == true)
                {
                    DestroyImmediate(gameObject);
                }
            }

            objectContainer = null;
        }
    }
}
